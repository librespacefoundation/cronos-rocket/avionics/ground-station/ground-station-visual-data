# :chart_with_upwards_trend: Telemetry GUI

Utilizes OpenMCT (a NASA web framework) in order to monitor all of the measurements that we receive from the sensors in real time.

### :file_folder: Structure
```
ground-station-visual-data/
├── public/
│   └── index.html
├── src/
│   ├── displays/
│   │   └── cronos_display_layout.json
│   ├── lib/
│   │   └── http.js
│   ├── server/
│   │   ├── server.js
│   │   ├── static-server.js
│   │   ├── realtime-server.js
│   │   ├── history-server.js
│   │   └── rocket.js
│   ├── realtime-telemetry-plugin.js
│   ├── historical-telemetry-plugin.js
│   ├── dictionary-plugin.js
│   └── dictionary.json
├── package.json
├── .gitignore
├── .npmignore
└── README.md
```

### :nut_and_bolt: Setup
**Prerequisites**
- [node.js](https://nodejs.org/en/)
- [git](https://git-scm.com/)


Use the terminal for the following steps:
```bash
git clone https://gitlab.com/librespacefoundation/cronos-rocket/avionics/ground-station/ground-station-visual-data.git
cd ground-station-visual-data
npm install
```

###  :joystick: Usage

Use the terminal inside the ```ground-station-visual-data/``` folder to start the deployment server using the following command:
```bash
npm start
```
and click [here](http://127.0.0.1:8080/) to access it.
