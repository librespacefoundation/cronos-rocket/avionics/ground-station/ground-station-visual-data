/*
 Rocket.js simulates a rocket generating telemetry.
*/

function Rocket() {
    this.state = {
        "metric.pressureA": 77,
        "metric.pressureB": 77,
        "metric.pressureC": 77,
        "metric.altitude": 100,
        "metric.velocity": 8.15,
        "metric.acceleration": 8.15,
        "metric.voltage": 30,
        "prop.valve": "OFF",
        "comms.recd": 0,
        "comms.sent": 0,
    };
    this.history = {};
    this.listeners = [];
    Object.keys(this.state).forEach(function (k) {
        this.history[k] = [];
    }, this);

    setInterval(function () {
        this.updateState();
        this.generateTelemetry();
    }.bind(this), 1000);

    console.log("Rocket launched!");
    console.log("Press Enter to toggle valve state.");

    process.stdin.on('data', function () {
        this.state['prop.valve'] =
            (this.state['prop.valve'] === "OFF") ? "ON" : "OFF";
        this.state['comms.recd'] += 32;
        console.log("Valve " + this.state["prop.valve"]);
        this.generateTelemetry();
    }.bind(this));
};

Rocket.prototype.updateState = function () {
    this.state["metric.pressureA"] = Math.max(
        0,
        this.state["metric.pressureA"] -
            (this.state["prop.valve"] === "ON" ? 0.5 : 0)
    );
        + Math.random() * 0.25 + Math.sin(Date.now());
    this.state["metric.pressureB"] = Math.max(
        0,
        this.state["metric.pressureB"] -
            (this.state["prop.valve"] === "ON" ? 0.5 : 0)
    );
        + Math.random() * 0.25 + Math.sin(Date.now());
    this.state["metric.pressureC"] = Math.max(
        0,
        this.state["metric.pressureC"] -
            (this.state["prop.valve"] === "ON" ? 0.5 : 0)
    );
        + Math.random() * 0.25 + Math.sin(Date.now());
    
    if (this.state["prop.valve"] === "ON") {
        this.state["metric.velocity"] = 8.15;
    } else {
        this.state["metric.velocity"] = this.state["metric.velocity"] * 1.125;
    }
    this.state["metric.altitude"] = this.state["metric.altitude"] * 0.985;
    this.state["metric.acceleration"] = this.state["metric.acceleration"] + Math.pow(Math.random(), 2);
    this.state["metric.voltage"] = 30 + Math.pow(Math.random(), 3);
};

/**
 * Takes a measurement of rocket state, stores in history, and notifies 
 * listeners.
 */
Rocket.prototype.generateTelemetry = function () {
    var timestamp = Date.now(), sent = 0;
    Object.keys(this.state).forEach(function (id) {
        var state = { timestamp: timestamp, value: this.state[id], id: id};
        this.notify(state);
        this.history[id].push(state);
        this.state["comms.sent"] += JSON.stringify(state).length;
    }, this);
};

Rocket.prototype.notify = function (point) {
    this.listeners.forEach(function (l) {
        l(point);
    });
};

Rocket.prototype.listen = function (listener) {
    this.listeners.push(listener);
    return function () {
        this.listeners = this.listeners.filter(function (l) {
            return l !== listener;
        });
    }.bind(this);
};

module.exports = function () {
    return new Rocket()
};
