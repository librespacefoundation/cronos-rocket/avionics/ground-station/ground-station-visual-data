var Rocket = require('./rocket');
var RealtimeServer = require('./realtime-server');
var HistoryServer = require('./history-server');
var StaticServer = require('./static-server');

var expressWs = require('express-ws');
var app = require('express')();
expressWs(app);

var rocket = new Rocket();
var realtimeServer = new RealtimeServer(rocket);
var historyServer = new HistoryServer(rocket);
var staticServer = new StaticServer();

app.use('/realtime', realtimeServer);
app.use('/history', historyServer);
app.use('/', staticServer);

var PORT = process.env.PORT || 3000;
var HOST = '0.0.0.0';

app.listen(PORT, HOST, function () {
    console.log('Open MCT hosted at http://localhost:' + port);
    console.log('History hosted at http://localhost:' + port + '/history');
    console.log('Realtime hosted at ws://localhost:' + port + '/realtime');
});
