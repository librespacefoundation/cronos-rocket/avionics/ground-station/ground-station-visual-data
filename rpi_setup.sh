# install text editor
apt install vim -y

# Install git
apt install git -y

# ------------------------------------------
# Install nvm
# ------------------------------------------
cd /home/pi

git clone https://github.com/nvm-sh/nvm.git .nvm

cd /home/pi/.nvm

git checkout v0.38.0

source ./nvm.sh

export NVM_DIR="/home/pi/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh" # This loads nvm

cat >> /home/pi/.bashrc <<EOL
export NVM_DIR=/home/pi/.nvm
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh" # This loads nvm
EOL

echo 'NVM version' $(nvm -v)

# Install the desired version of nodejs
nvm install v12.21.0

echo 'NODE version' $(node -v)

# ------------------------------------------
# Install docker
# ------------------------------------------

# Downloading the installation script
curl -sSL https://get.docker.com | sh

# Adding a Non-Root User to the Docker Group
usermod -aG docker pi

# Verify that Docker Engine is installed correctly
docker run hello-world

apt install -y libffi-dev libssl-dev

apt install -y python3 python3-pip

apt remove python-configparser

# Install docker-compose
pip3 -v install docker-compose

# ------------------------------------------

# Change all the permissions inside a directory
mkdir /home/pi/apps
chown pi:pi -R /home/pi/apps

#---------------------------------------------------------

# Enable automatic loading of i2c kernel module
sed -i 's/#dtparam=i2c_arm=on/dtparam=i2c_arm=on/g' /boot/config.txt

# Disable power saving on LAN (eth0) interface.
sed -i -e '$adtparam=eee=off' /boot/config.txt

# Disable power saving on WiFi (wlan0) interface
sed -i -e 's/exit 0//g' /etc/rc.local
sed -i -e '$a/sbin/iwconfig wlan0 power off' /etc/rc.local
sed -i -e '$aexit 0' /etc/rc.local

iw wlan0 get power_save

# Turn wifi on  
rfkill unblock wifi
rfkill list all

#---------------------------------------------------------
# Set up Raspberry Pi to match your regional settings
#---------------------------------------------------------

# Define wireless LAN country code
cat >> /etc/wpa_supplicant/wpa_supplicant.conf <<EOL
country=GR
network={
    ssid="Nova-6696A9"
    psk="45674567kK"
}
EOL

# To load after editing the config file
wpa_cli -i wlan0 reconfigure

# You can verify whether it has successfully connected using ifconfig wlan0.
# If the inet addr field has an address beside it, the Raspberry Pi has connected to the network.
ifconfig wlan0
iwlist wlan0 scan | grep ESSID

sed -i "s/.*/Europe\/Athens/" /etc/timezone
sed -i '/en_GB.UTF-8 UTF-8/s/^# //g' /etc/locale.gen
sed -i '/SendEnv LANG LC_*/s/^/#/g' /etc/ssh/ssh_config

echo "LC_ALL=en_GB.UTF-8" | tee -a /etc/environment > /dev/null
echo "LANG=en_GB.UTF-8" | tee /etc/locale.conf > /dev/null

locale-gen en_GB.UTF-8

cat >> /etc/default/locale <<EOL
LANGUAGE=en_GB.UTF-8 # List of fallback message translation languages (GNU only)
LC_ALL=en_GB.UTF-8 # Overrides all other locale variables (except LANGUAGE)
EOL

cat >> /home/pi/.bashrc <<EOL
export LANGUAGE=en_GB.UTF-8 # List of fallback message translation languages (GNU only)
export LC_ALL=en_GB.UTF-8 # Overrides all other locale variables (except LANGUAGE)
EOL

source ~/.bashrc

locale-gen

#---------------------------------------------------------

# Setup UFW Firewall
apt install ufw -y
ufw enable
ufw allow ssh

# systemctl enable ssh

# Installing fail2ban
apt install fail2ban -y
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

cat >> /etc/fail2ban/jail.local <<EOL
[ssh]
enabled  = true
port     = ssh
filter   = sshd
logpath  = /var/log/auth.log
maxretry = 6
EOL

# ------------------------------------------
# Setting up a Routed Wireless Access Point
# ------------------------------------------

# Install hostapd access point software package
apt install hostapd -y

# Enable the wireless access point service and set it to start when your Raspberry Pi boots
systemctl unmask hostapd
systemctl enable hostapd

# In order to provide network management services (DNS, DHCP) to wireless clients,
# Install dnsmasq software package, in order to provide network management services (DNS, DHCP) to wireless clients.
apt install dnsmasq -y

# install netfilter-persistent and its plugin iptables-persistent, to save firewall rules and restore them when the Raspberry Pi boots.
DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent

# Configure the static IP address
echo "interface wlan0" >> /etc/dhcpcd.conf
echo "    static ip_address=192.168.4.1/24" >> /etc/dhcpcd.conf
echo "    nohook wpa_supplicant" >> /etc/dhcpcd.conf

# Configure the DHCP and DNS services for the wireless network
mv /etc/dnsmasq.conf /etc/dnsmasq.coinf.orig
cat >> /etc/dnsmasq.conf <<EOL
interface=wlan0 # Listening interface
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h # Pool of IP addresses served via DHCP
domain=wlan # Local wireless DNS domain
address=/gw.wlan/192.168.4.1 # Alias for this router
EOL

# Ensure Wireless Operation
rfkill unblock wlan

# Configure the AP Software
cat >> /etc/hostapd/hostapd.conf <<EOL
country_code=GR
interface=wlan0
ssid=CronosControlBox
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=cronoscb
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
EOL

systemctl reboot
