import matplotlib.pyplot as plt
import matplotlib.animation as animation
# from matplotlib import style
import socket
import sys
import getopt
import numpy as np

from help import split_data, data, TOTAL_POINTS_PER_GRAPH

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", ["rpip_addr="])

    except getopt.GetoptError:
        print("the correct format is RealTimePlot_and_Socket.py --rpip_addr=<rpi ip address>")
        sys.exit(2)

    if len(opts) == 0:
        print("you must specify rpip_addr long option")
        sys.exit(3)

    rpip_addr = None

    for i in opts:
        if i[0] == "--rpip_addr":
            rpip_addr = i[1]

    TCP_IP = str(rpip_addr)  # rasberry ip
    TCP_PORT = 8080
    BUFFER_SIZE = 1024
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
  
    split_data(s.recv(BUFFER_SIZE).decode() ) 
    
    # import matplotlib.pyplot as plt
    # import numpy as np

    x = np.linspace(0, TOTAL_POINTS_PER_GRAPH, TOTAL_POINTS_PER_GRAPH)
    y = [0 for i in range(len(x))]

    plt.ion()
    fig = plt.figure("Load Cells")
    ax = fig.add_subplot(111)
    plt.title('Load Cells')
    plt.legend(['run tank'])
    line1, = ax.plot(x, y, 'b-')

    while True: 
        dict = split_data(s.recv(BUFFER_SIZE).decode() )    
        t = list(np.zeros(TOTAL_POINTS_PER_GRAPH - len(data['lc_r'])))        
        
        for i in range(len(data['lc_r'])):
            t.append(data['lc_r'][i])
         
        ax.plot(t)
        fig.canvas.draw()
        fig.canvas.flush_events()
    # fig = plt.figure("Load Cells")
    # ax = fig.add_subplot(111)  
    # line1, = ax.plot(data['lc_r'], 'b-')

    # for i in range(5000):
    #     split_data(s.recv(BUFFER_SIZE).decode() ) 

    #     line1.set_ydata(data['lc_r'])
    #     fig.canvas.draw()
    #     fig.canvas.flush_events()
        # plt.clf() 
        # plt.plot(data['lc_r'])
        # plt.plot(data['lc_e1'])
        # plt.plot(data['lc_e2'])
        # plt.plot(data['lc_e3']) 
        # plt.legend(["runtank", "engine1", "engine2", "engine3"])
        # plt.pause(0.005)

    # plt.show()

    #
    # print("lc_e1 = ")
    # for i in range(len(lc_e1)):
    #     print()

    # print(command)



    # global counter_lc
    #
    # def animate():
    #     # global counter_lc
    #
    #
    #     global counter
    #
    #     command = s.recv(BUFFER_SIZE)
    #     command = command.decode()
    #     split_data(command)

    #
    #
    #     # if counter == 1000:
    #     #     xs.pop(0)
    #     #     ys.pop(0)
    #     #
    #     # xs.append(counter)
    #     # ys.append(command)
    #     #
    #     # plt.clf()
    #     # plt.plot(xs, ys)
    #     # plt.title("")
    #     # plt.xlabel("")
    #     # plt.ylabel("")
    #
    # ani = animation.FuncAnimation(fig1, animate, interval=100)
    # plt.show()

    s.close()

# TCP_IP = '127.0.0.1' #rasberry ip
# TCP_PORT = 5005
# BUFFER_SIZE = 1024
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.connect((TCP_IP, TCP_PORT))
# data = s.recv(BUFFER_SIZE)

# style.use('fivethirtyeight')
#
# fig = plt.figure()
# ax1 = fig.add_subplot(1,1,1)

# def animate(i):
#
#
#     xs = []
#     ys = []
#
#     ax1.clear()
#     ax1.plot(xs, ys)
#
# ani = animation.FuncAnimation(fig, animate, interval=1000)
# plt.show()

if __name__ == "__main__":
    # print(sys.argv)
    main(sys.argv[1:])
