#!/usr/bin/python3

import socket 
import _thread as thread
import time

TOTAL_SOCKETS = 10
port = 8080

client_sockets = []
new_value = ""

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind( ('', port))
    s.listen(TOTAL_SOCKETS)
except socket.error as err:
    print("Error creating socket")
    quit()

print("Socket was created successfully")


def accept_new_clients():
    try:
        while True:
            print("accpet clients")
            c, addr = s.accept()     
            client_sockets.append(c)   
    except Exception as e:
        print(e)

    return None
 
def write_to_clients():
    try:
        global new_value
        while True:
            if len(new_value) > 0 and len(client_sockets) > 0: 
                
                to_be_removed = []
                for c in client_sockets:
                    try:
                        c.send(new_value.encode())
                        # print("Transmited: ", new_value)
                    except Exception as e:
                        to_be_removed.append(c)
                        print("error on send: ", e, client_sockets) 
                
                for c in to_be_removed:
                    client_sockets.remove(c)
                    
                new_value = "" 
            
    except Exception as e:
        print("Write: ", e)
            
try:
    thread.start_new_thread(accept_new_clients, ())
    thread.start_new_thread(write_to_clients, ())
except Exception as e:
    print(e)
    print("error on thread")


import random
while True: 
    
    new_value = f'''loadCell r: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***loadCell e1: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***loadCell e2: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***loadCell e3: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***msp r: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***msp i: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***msp c: {round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)},{round(random.random() * 50,3)}***pot f: {random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)}***pot t: {random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)}***pot p: {random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)},{random.randint(0,4000)}***motor f: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***motor q: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***motor t: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***motor p: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***motor r: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***progress: {random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)},{random.randint(0,6)}***'''
    # s = split_data(new_value)
    # from pprint import pprint
    # pprint(s)

    time.sleep(0.1) 
