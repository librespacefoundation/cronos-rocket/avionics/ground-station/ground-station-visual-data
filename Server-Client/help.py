TOTAL_POINTS_PER_GRAPH = 500

data = {
    "lc_r": [],
    "lc_e1": [],
    "lc_e2": [],
    "lc_e3": [],
    "msp_r": [],
    "msp_i": [],
    "msp_c": [],
    "pot_f": [],
    "pot_t": [],
    "pot_p": [],
    "motor_q": [],
    "motor_f": [],
    "motor_t": [],
    "motor_p": [],
    "motor_r": [],
    "progress": []
}  


def split_data(data_): 
    dict = {
        "lc_r": [],
        "lc_e1": [],
        "lc_e2": [],
        "lc_e3": [],
        "msp_r": [],
        "msp_i": [],
        "msp_c": [],
        "pot_f": [],
        "pot_t": [],
        "pot_p": [],
        "motor_q": [],
        "motor_f": [],
        "motor_t": [],
        "motor_p": [],
        "motor_r": [],
        "progress": []
    }  

    for d in data_.split('***'):
        if d == '':
            continue
        try:
            sensor = d.split(': ') 
            type = sensor[0].split(' ')
            all = sensor[1].split(',') 
            if type[0] == 'loadCell': 
                if type[1] == 'r':
                    for s in all:
                        dict["lc_r"].append(round(float(s),3)) 
                elif type[1] == 'e1':
                    for s in all:
                        dict["lc_e1"].append(round(float(s),3))
                elif type[1] == 'e2':
                    for s in all:
                        dict["lc_e2"].append(round(float(s),3))
                elif type[1] == 'e3':
                    for s in all:
                        dict["lc_e3"].append(round(float(s),3))

            elif type[0] == 'msp':
                if type[1] == 'r':
                    for s in all:
                        dict["msp_r"].append(round(float(s),3))
                elif type[1] == 'i':
                    for s in all:
                        dict["msp_i"].append(round(float(s),3))
                elif type[1] == 'c':
                    for s in all:
                        dict["msp_c"].append(round(float(s),3))

            elif type[0] == 'motor':
                if type[1] == 'f':
                    for s in all:
                        dict["motor_f"].append(round(int(s)))
                elif type[1] == 'q':
                    for s in all:
                        dict["motor_q"].append(round(int(s)))
                elif type[1] == 't':
                    for s in all:
                        dict["motor_t"].append(round(int(s)))
                elif type[1] == 'p':
                    for s in all:
                        dict["motor_p"].append(round(int(s)))
                elif type[1] == 'r':
                    for s in all:
                        dict["motor_r"].append(round(int(s)))
                    
            elif type[0] == 'progress':
                for s in all:
                    dict["progress"].append(round(int(s)))
            
            elif type[0] == 'pot':
                if type[1] == 'f':
                    for s in all:
                        dict["pot_f"].append(round(int(s)))
                elif type[1] == 't':
                    for s in all:
                        dict["pot_t"].append(round(int(s)))
                elif type[1] == 'p':
                    for s in all:
                        dict["pot_p"].append(round(int(s)))
        except Exception as e:
            print(e)

            from time import sleep
            print("D: ", d) 
            
            
    for d in dict: 
        data[d].extend(dict[d])
        if len(data[d]) > TOTAL_POINTS_PER_GRAPH:
            data[d] = data[d][-TOTAL_POINTS_PER_GRAPH:]
            
    
    return dict